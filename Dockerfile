#stage 1
FROM node:12 as builder
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build --prod
#stage 2
FROM nginx:alpine
COPY --from=builder /app/dist/employeemanagerapp /usr/share/nginx/html
